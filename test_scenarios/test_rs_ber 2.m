inStruct.SNRdB=1:6;
inStruct.modulationOrder=2;
inStruct.N_msg=1e3;
inStruct.noisePower=0.01;
inStruct.AlphabetSize = 8;
inStruct.MessageLength = [245,225,205,165];


%turn off all the rest

%saeed: these internal variables must be kept internal. either turn it into a
%function or what?

% To add the directory where the necessary functions are
TopFolder = fileparts(pwd);
addpath(genpath([TopFolder, '/functions']));

Modulation={'BPSK','QAM 4','QAM 8','QAM 16','QAM 32','QAM 64','QAM 128','QAM 256'};


BER = zeros(length(inStruct.SNRdB),length(inStruct.MessageLength));
SER = zeros(length(inStruct.SNRdB),length(inStruct.MessageLength));
FER = zeros(length(inStruct.SNRdB),length(inStruct.MessageLength));
FER_ref = zeros(length(inStruct.SNRdB),length(inStruct.MessageLength));

num_correctable_errors = floor((2^inStruct.AlphabetSize-1 - inStruct.MessageLength)/2);
numBits = inStruct.N_msg*inStruct.MessageLength*inStruct.AlphabetSize;

for i_SNR = 1:length(inStruct.SNRdB)
    for i = 1:length(inStruct.MessageLength) 

        InStream_bit = randi([0,1],[1,numBits(i)]);

        % TX        
        %       ENCODING: k*m message bits -> n*m codeword bits
                cw = encoder_rs(InStream_bit, inStruct.AlphabetSize, inStruct.MessageLength(i)); 
                
        %       MODULATION: log2(modulationOrder) bits -> 1 complex symbol 
                sym_num=bit2symnum(cw,inStruct.modulationOrder); % Each symbol is represented by a decimal number from 0 to modulationOrder-1.
                sym=bit2sym(inStruct.modulationOrder,cw); % Each symbol is represented by a complex number. 
                signal_power=10.^(inStruct.SNRdB(i_SNR)/10)*inStruct.noisePower; %*log2(inStruct.modulationOrder);
                Scaled_signal=sqrt(signal_power)*sym;
                %scatterplot(Scaled_signal); 
                
        % CHANNEL 
                noise=sqrt(inStruct.noisePower)*sqrt(0.5)*(randn(1,length(sym))+1i*randn(1,length(sym)));
                noisy=noise'+Scaled_signal';
                %scatterplot(noisy)
                
        % RX 
        %       DETECTION: 1 complex symbol -> log2(modulationOrder) bits
                detected = min_distance_detection(noisy,inStruct.modulationOrder,signal_power);
                bit = sym2bit(detected-1,inStruct.modulationOrder);

                if numel(bit) ~= numel(cw)
                    disp("Some bits for the last block (codeword) are not sent as the bit stream is not a multiple of log2(modulation order).\n")
                    disp("The last message will be ignored for the error analysis.")
                    inStruct.N_msg = inStruct.N_msg - 1;
                end

        %       DECODING: n*m codeword bits -> k*m message bits
                bit_dec = decoder_rs(bit(1:inStruct.N_msg*(2^inStruct.AlphabetSize-1)*inStruct.AlphabetSize), inStruct.AlphabetSize, inStruct.MessageLength(i));      

        %       ERROR PROBABILITY 
                BER(i_SNR,i)=sum(InStream_bit(1:length(bit_dec))~=bit_dec') / length(bit_dec); % Bit Error Rate
                SER(i_SNR,i)=(sum(detected-1~=sym_num'))/length(detected); % Symbol Error Rate       
                FE = 0; 
                for j = 1:inStruct.N_msg
                    FE = FE +1 - isequal(bit_dec((j-1)*inStruct.MessageLength(i)+1:j*inStruct.MessageLength(i))'>0.5,InStream_bit((j-1)*inStruct.MessageLength(i)+1:j*inStruct.MessageLength(i))>0.5);
                end
                FER(i_SNR,i)=FE/inStruct.N_msg; %Frame (Block) Error Rate 
                
                FE_ref = 0; 
                
                for j = 1:inStruct.N_msg
                    num_bit_errors = sum((bit((j-1)*inStruct.MessageLength(i)+1:j*inStruct.MessageLength(i))'>0.5)~=(InStream_bit((j-1)*inStruct.MessageLength(i)+1:j*inStruct.MessageLength(i))>0.5),'all');
                    FE_ref = FE_ref + (num_bit_errors > num_correctable_errors(i));                   
                end
                FER_ref(i_SNR,i)=FE_ref/inStruct.N_msg;
        
    end
end


outStruct.BER = BER;
outStruct.SER = SER;
outStruct.FER = FER;

refStruct.FER = FER_ref;


figure
semilogy(inStruct.SNRdB,outStruct.BER,'-o')
str_n = ['(', num2str(2^inStruct.AlphabetSize-1), ','];%, num2str(inStruct.MessageLength), ')'];
str_lgd = strings(length(inStruct.MessageLength),1);
for i = 1:length(inStruct.MessageLength)
    str_lgd(i) = [str_n,  num2str(inStruct.MessageLength(i)), ')-RS'];
end
legend(str_lgd)
xlabel('SNR [dB]')
ylabel('BER [dB]')
grid on
title(['Modulation' Modulation(log2(inStruct.modulationOrder))])

figure
semilogy(inStruct.SNRdB,outStruct.SER,'-o')
str_n = ['(', num2str(2^inStruct.AlphabetSize-1), ','];%, num2str(inStruct.MessageLength), ')'];
str_lgd = strings(length(inStruct.MessageLength),1);
for i = 1:length(inStruct.MessageLength)
    str_lgd(i) = [str_n,  num2str(inStruct.MessageLength(i)), ')-RS'];
end
legend(str_lgd)
xlabel('SNR [dB]')
ylabel('SER [dB]')
grid on
title(['Modulation' Modulation(log2(inStruct.modulationOrder))])

figure
semilogy(inStruct.SNRdB,outStruct.FER,'-o',inStruct.SNRdB,refStruct.FER, '-x')

str_n = ['(', num2str(2^inStruct.AlphabetSize-1), ','];%, num2str(inStruct.MessageLength), ')'];
str_lgd = strings(length(inStruct.MessageLength)*2,1);
for i = 1:length(inStruct.MessageLength)
    str_lgd(i) = [str_n,  num2str(inStruct.MessageLength(i)), ')-RS'];
    str_lgd(i+length(inStruct.MessageLength)) = [str_n,  num2str(inStruct.MessageLength(i)), ')-RS Bound'];
end
legend(str_lgd)
xlabel('SNR [dB]')
ylabel('FER [dB]')
grid on
title(['Modulation' Modulation(log2(inStruct.modulationOrder))])



